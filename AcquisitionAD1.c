
/*
 * PmodAD1.c
 *
 *  Created on: 30 avr. 2020
 *      Author: JB
 *    How to use the PMODAD1 ACS with a Basys3 board using Xilinx IP.  Training BTS Chevrollier.
 */



#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "PmodAD1.h"
#include "PmodDA1.h"
#include "sleep.h"
#include "xil_cache.h"
#include "xil_io.h"
#include "xil_types.h"
#include "xparameters.h"
#include"xgpio.h"
#include "xstatus.h"
#include "xintc.h"
#include "xil_exception.h"



// variables globales
PmodAD1 myDevice;
XGpio led;
XGpio marqueur;
AD1_RawData RawData;
AD1_PhysicalData PhysicalData;
u32 X0;

// prototypes de fonction
void InitADC();
void AcquisitionAffichage();
void ArretProgramme();
void EnableCaches();
void DisableCaches();


int main() {


/**** INITIALISATION DES LEDS ****/
	XGpio_Initialize(&led, XPAR_AXI_GPIO_0_DEVICE_ID);  // Initialisation du GPIO LEDs
	XGpio_SetDataDirection(&led, 2, 0x00000000); // Paramétrage des Leds en sortie
/*********************************/
	InitADC();   // initialisationd l'ADC

	   xil_printf("debut de programme\n\r");       // vérification du bon lancement du programme
	   while(1){
		   AcquisitionAffichage(); // acquisition en boucle infinie
	   }
	   ArretProgramme();   // arrêt du programme  et vider les caches.
   return 0;
}

void AcquisitionAffichage() {
/*** ACQUISITION ADC *************/
  X0=AD1_GetSample(&myDevice, &RawData); // Echantillonnage et conversion ADC
  xil_printf("valeur mesuree:%d\r\n",X0);  // impression sur un terminal de la valeur numérique mesurée
  XGpio_DiscreteWrite(&led, 2, X0);// valeur des leds à l'instant t
  usleep(2000000);                          // mise en place d'une temporisation

}



void InitADC() {
   AD1_begin(&myDevice, XPAR_PMODAD1_0_AXI_LITE_SAMPLE_BASEADDR);  // echantillonnage et conversion du signal analogique sur A0 et A1
   	   	   	   	   	   	   	   	   	   	   	   	   	   	   	   	   // attente d'une microsecond pour la mise sous tension de l'ADC
   usleep(1); 														// 1 us (minimum)
}



void ArretProgramme() {
	DisableCaches();
}



